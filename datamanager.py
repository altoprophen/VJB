'''
database manager

'''

import dbengine,os,pprint,datetime

#   first some globals
vids_location = dbengine.location

loA = dbengine.Accessor()
db = loA.get_database()

pp = pprint.PrettyPrinter(indent=2)

if  len(db['canon']) == 0:
    #   if the size of the canonical list of songs is == 0
    #   there are no songs in the database, we need to create the list
    
    #first create the list of files
    print 'loc: ',
    pp.pprint(vids_location)
    os.chdir(vids_location)#gotta do this or os.path.isfile (below) won't work

    files = filter(os.path.isfile, os.listdir(vids_location) )
    print 'files: ',
    pp.pprint(files)
    for file in files:
        #   add this file to the master datbase
        vid = dbengine.Video(file,datetime.datetime.fromtimestamp(os.path.getmtime(file)))
        vid.index = len(db['canon'])
        db['canon'].append(vid)
    #   re-save the entire database
    loA.set_database(db)
    
def re_index():
    '''
    this re-indexes the database
    what that means is it takes the canonical list and creates new lists, 
    ordered in the manner in which that kind of list is supposed to be ordered
    for example, it creates and orders the alphabetical index by
    artist and title in alphabetical order.
    '''
    db['indexes']['alpha'] = sorted(db['canon'], 
        key=lambda vid: vid.name)
    #   re-save the entire database
    loA.set_database(db)