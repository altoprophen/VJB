'''
the idle screen mananger

runs the idle mode command routine in a separate thread; it recieves commands
off the message bus (philbus) and relays commands onto a local message queue
'''

import subprocess
import time
import threading
import philbus
import Queue
import dbengine
import pprint
import random
import sys
import pickle
import pprint

# global
goMessageQueue = Queue.Queue()

# idle mode command module
def idleModeCmdRoutine():
   '''
   '''
   bus = philbus.Receiver()
   xmitter = philbus.Transmitter()
   global goMessageQueue
   for cmd in bus.listen():
      pp = pprint.PrettyPrinter(depth = 6,indent=4)
      #pp.pprint(cmd)
      try:
         cmdob = pickle.loads(cmd['data'])
      except (pickle.PickleError,TypeError):
         #   if we've reached here, unpickling was unsuccessful so the data from the bus is not a pickle
         if isinstance(cmd['data'],long):
            pass
         elif cmd['data'] == 'hi':
            pass
         continue   #   this line marks the end of this 'except' suite
      #    if we';'ve reached here there were no errors unpickling, so..
      #okay, now we get into the mode suites, 
      if cmdob.command == 'AIDLMD':
         '''
         if the command is to activate idle mode
         '''
         print 'AIDLMD'
         goMessageQueue.put("STRT",block=False)#   put a "STRT" message on the local message queue
      elif cmdob.command == 'DIDLMD':    
         '''
         if the command is to deactivate idle mode
         '''
         print 'DIDLMD'
         goMessageQueue.put("STOP",block=False)#   put a "STOP" message on the local message queue
         #print 'about to join'
#         loRandomVideoThread.join()
##         print 'about to exit IMCR'
##         break



def oneRandomVidAfterAnother():
   loA = dbengine.Accessor()
   db = loA.get_database()
   global goMessageQueue
   loPlayVideos = False
   loVlc = None
   while True:
      if loPlayVideos:
         print "in video playing part of loop"
         # pick a random video 
         loRandomVideo = random.choice(db['canon'])
         #  got this from player py
         loVlc = subprocess.Popen(["cvlc","--play-and-exit","-I","rc",    #	this one for the video jukebox with two monitors
         "--vout","x11","--x11-display",":0.1",
         "--width=1280","--height=1024",         
         "--no-audio", "--sub-filter=marq", "--marq-marquee","\$N",
         "--marq-position", "0", 
         loRandomVideo.fullpath])#,
#         stdout=subprocess.PIPE,
#         stderr=subprocess.PIPE,
#         stdin=subprocess.PIPE)

         # this is the one for testing
#         loVlc = subprocess.Popen(["vlc","--play-and-exit",    #	this one for the video jukebox with two monitors
#   #      "--vout","x11","--x11-display",":0.0",
#         "--no-audio", "--sub-filter=marq", "--marq-marquee", "\$N",
#         "--marq-position", "0", 
#         #"--width=1280","--height=1024",
#         loRandomVideo.fullpath],
#         stdout=subprocess.PIPE,
#         stderr=subprocess.PIPE,
#         stdin=subprocess.PIPE)
      
      #  now keep looking on the message queue to see if we have a status change request 
      #  and also keep checking if loVlc is still alive 
      while 1:
         try:
            lsMessage_off_queue = goMessageQueue.get_nowait()
            #break
            print "message:",lsMessage_off_queue
         except Queue.Empty:
            lsMessage_off_queue = ""            
            time.sleep(0.1)
         if lsMessage_off_queue == "STRT":
            loPlayVideos = True
         elif lsMessage_off_queue == "STOP":
            print "got STOP message,about to kill vlc"
            if loVlc is not None:
               loRetval = loVlc.poll()
               print 'retval', loRetval
               print 'returncode:',loVlc.returncode
               if loVlc.returncode is None:
                  loVlc.kill()
                  loVlc.communicate()# wait for it to end
                  print 'vlc killed'
            loPlayVideos = False
##            print 'account of threads'
##            pp = pprint.PrettyPrinter(depth = 6,indent=4)
##            pp.pprint(threading.enumerate())
            
##            print "about to exit random video manager"
##            raise SystemExit
##            print "just exited"
         
         if loVlc is not None:
            if (loVlc.poll() is not None) and loPlayVideos :# if there's a return code and we want to play more videos
               break
         else:
            if loPlayVideos:
               break

#   create and start the command loop
##cmdThrd = threading.Thread(target = idleModeCmdRoutine,name="IdleModeCommandRoutine")
##cmdThrd.start()

#if not loRandomVideoThread.isAlive():
loRandomVideoThread = threading.Thread(name="RandomVidPlayer",target=oneRandomVidAfterAnother)
loRandomVideoThread.start()
idleModeCmdRoutine()

#  execution probably never reaches down here now...
print 'about to exit program'
#sys.exit()
raise SystemExit
