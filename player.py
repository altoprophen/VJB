#!/usr/bin/env python
'''
#!/usr/bin/python

vjb version 2.0 player module 2013-1-10



was working on cmdloop() and the philbus module

archived it after ironinng out a bug, it now changes to stop mode regardless of 
whether the video stops playing all by itself or is stopped from the controller
'''

from Tkinter import *
import subprocess
import threading
#both these imported in philbus
#import redis
import pickle
import philbus
import vjbmm
import time
import pprint
import logging
from logging.handlers import SysLogHandler


    
class Application(Frame):
    def __init__(self, master=None):
        # before we do anything else, create logger
        self.logger = logging.getLogger('vjb-player')
        self.logger.setLevel(logging.DEBUG)
        # create handler and set level to debug
        ch = logging.handlers.SysLogHandler()
        ch.setLevel(logging.DEBUG)
        # create formatter
        formatter = logging.Formatter('%(pathname)s [%(process)d] : %(message)s')
        # add formatter to ch
        ch.setFormatter(formatter)
        # add ch to logger
        self.logger.addHandler(ch)

        Frame.__init__(self, master)
        self.grid()
        self.createWidgets()
        #   create the bus objects
        self.bus = philbus.Receiver()
        self.xmitter = philbus.Transmitter()
        
        #   create and start the command loop thread
        self.cmdThrd = threading.Thread(target = self.cmdloop)
        self.cmdThrd.setDaemon(True)
        self.cmdThrd.start()

    def createWidgets(self):
        self.quitButton = Button ( self, text='Quit',
            command=self.quit )
        self.mycanvas = Canvas(self,height=80, width=80)
        self.quitButton.grid()
        self.mycanvas.grid(column=1,row=0,rowspan=3)
        
    def cmdloop(self):
        '''
        this is started and run in an independent thread
        
        '''
        submode0 = vjbmm.SMM()
        submode0.changeMode('STPM')#    before this is ever queried, make sure it is in the default mode
        for cmd in self.bus.listen():
            pp = pprint.PrettyPrinter(depth = 6,indent=4)
            #pp.pprint(cmd)
            try:
                cmdob = pickle.loads(cmd['data'])
            except (pickle.PickleError,TypeError):
                #   if we've reached here, unpickling was unsuccessful so the data from the bus is not a pickle
                self.logger.debug('pickle error!')#print 'pickle error!'
                if isinstance(cmd['data'],long):
                    self.logger.debug('not Song, long')
                elif cmd['data'] == 'hi':
                    self.logger.debug("cmd['data'] == 'hi'")
                else :
                    self.logger.debug('default')
                continue   #   this line marks the end of this 'except' suite
            #    if we';'ve reached here there were no errors unpickling, so..
            #okay, now we get into the mode suites, 
            if submode0.currModeShortName() == 'STPM':
                #   all code in this suite executes if this process is in submode 0's 'stop mode'
                # print 'STPM'
                if cmdob.command == 'play':
                    # if the play command was issued then:
                    self.logger.debug('STPM play %s', cmdob.song.fullpath)
                    #   execute the player in a separate process
                    #self.vlc = subprocess.Popen(["vlc",'--play-and-exit','--osd','-f',
                    #self.vlc = subprocess.Popen(["cvlc",'--novideo','--play-and-exit','--osd',"-I","rc",    #   this one for production systems with one screen; no video (audio jukebox)
                    #self.vlc = subprocess.Popen(["cvlc",'--play-and-exit','--osd',"-I","rc",#   the vlc for testing
                    self.vlc = subprocess.Popen(["cvlc","--play-and-exit","-I","rc",    #	this one for the video jukebox with two monitors, this was the production version
                    "--vout","x11","--x11-display",":0.1",
                    "--width=1024","--height=768",
                    cmdob.song.fullpath],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    stdin=subprocess.PIPE)
                    def playerMon():
                        '''
                        this little function is run in a separate thread, it
                        will monitor the player's process, periodically
                        polling it (every second) to see if it's still in existence, 
                        when it ceases to be, this thread will put a command on the 
                        wire, actually, an event notification (my first one!) 
                        notifying the entire system that vlc has ceased to exist.
                        it will then cause itself to expire after this.
                        
                        '''
                        while not self.vlc.poll():
                            #   execute this block while vlc is running
                            time.sleep(1)
                            self.logger.debug('playermon:tick!')
                        #   if we've reached here, vlc has ceased to exist
                        self.logger.debug('playermon:vlc stopped!')
                        #   phil-this should fix it!
                        submode0.changeMode('STPM')
                        nextcmd = philbus.Command('vlcs')#   make the command, or more properly, event notification
                        nextcmd.source = 'plr plym stop (playermon())'
                        self.xmitter.publish(nextcmd)#  put it on the wire
                    #   now set up the function to run in it's own thread and strt it
                    self.vlcmon = threading.Thread(target = playerMon)
                    self.vlcmon.start()
                    
                    submode0.changeMode('PLYM')
## phil-just commented this out 2013-5-4                        
##                    #   issue a command to change submode0 to play mode
##                    nextcmd = philbus.Command('chm0','PLYM')#   make the command
##                    nextcmd.source = 'plr stpm play (just started playermon())'
##                    self.xmitter.publish(nextcmd)#  put it on the wire
                elif cmdob.command == 'chm0':
                    #   if a command to change mode0
                    self.logger.debug(' chm0')
                    if cmdob.mode == 'PLYM':
                        self.logger.debug(' processing cmd to change to PLYM')
                        submode0.changeMode('PLYM')
            elif submode0.currModeShortName() == 'PLYM':
                #   and all the code in this suite executes if submode0 is in 'play mode'
                #logger.debug('PLYM')
                if cmdob.command == 'stop': #   if we got a 'stop' playback command off the wire
                    self.logger.debug('PLYM stop (issuing command to stop (kill vlc process))')
                    self.vlc.kill()
                    #   it will wait for vlc to actually stop and the event handler for that will change to stop mode
##                    #   issue a command to change submode0 to stop mode
##                    nextcmd = philbus.Command('chm0','STPM')#   make the command
##                    self.xmitter.publish(nextcmd)#  put it on the wire
                elif cmdob.command == 'chm0':#   if this is a command to change mode0
                    self.logger.debug('PLYM chm0')
                    if cmdob.mode == 'STPM':
                        self.logger.debug('PLYM processing cmd to change to STPM')
                        submode0.changeMode('STPM')
                elif cmdob.command == 'vlcs':#   if this is an event notiification that vlc has actually stopped (either from command or all by itself)
                    self.logger.debug('PLYM vlcs')
                    #   it will wait for vlc to actually stop and the event handler for that will change to stop mode
                    nextcmd = philbus.Command('chm0','STPM')#   make the command
                    nextcmd.source = 'plr plym vlcs'
                    self.xmitter.publish(nextcmd)#  put it on the wire
                    #   change to stop mode
                    
                
                
##            #   put the trace code here    
##            self.logger.debug('cmdob.command = %s',cmdob.command)
##            if cmdob.command == 'chm0':
##                self.logger.debug(cmdob.mode)
##            else:
##                pass
##                #print '',
##            try:
##                self.logger.debug('src= %s',cmdob.source)
##            except AttributeError:
##                self.logger.debug(' no source')
        
        
        

app = Application()
app.master.title("Player")
t = app.winfo_toplevel()
t.withdraw()

app.mainloop()

'''
todo:
I once manually killed the player I believe.. it left vlc running and so gave me an error on
subsequent runs when it tried to kill vlc itself.. delete this note if it makes no sense tomorrow!
'''
