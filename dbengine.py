'''
dbengine
a module to pickle and unpickle the object database at the heart
of the VJB

'''

import datetime,pickle

# some globals
location = '/home/juke/videos/'
dblocation = '/home/juke/Db.pkl'

class Video(object):
    '''
    succeessor the the Song object of the previous version. this object
    is the datum which represents a music video.
    it is at the heart of the database which is at the heart of the master 
    song list.
    
    has several attributes:
    artist
        Artist (list, the members of which refernce the external artist list)
    title
        Title (list, it's a list of stings, the video can feature more than one 
        song, this way you can refer to all of them)
    name
        Name on screen (automatically generated, has a method that does this)
    diskname
        Name on disk
    genera
        Genera (list with refernces to the Genera list)
    date
        file date (the file's mtime, stored as a datetime object)
    year
        Song Year (year song dropped)
    month
        Month (optional month song dropped)
    notes
        miscellaneous notes about the song that should show up in
        the song's name on screen
    index
        the file's index in the master list (db['canon'])
    '''
    def __init__(self,name,mtime):
        self.artist = []
        self.title = []
        self.genera = []
        self.diskname = name#   name on disk
        self.date = mtime   #   the file's mtime, serves as the date added
        self.year = ""
        self.month = ""
        self.notes = ""
        self.index = 0
        
    def get_name(self):
        '''
        property gettor for the name property
        
        if either the artist or title fields are blank then just use the 
        name on disk for this
        
        <here>
        '''
        if len(self.artist) == 0 or len(self.title) == 0:
            return self.diskname
    name = property(get_name,"the generated name of the video")
    
    def get_fullpath(self):
        '''
        property gettor for th fullpath property
        returns the full pathname of the file that plays this video
        '''    
        return location+self.diskname
    fullpath = property(get_fullpath,
        'return the full pathname of the file that plays this video')
    

class Artist(list):
    '''
    this is the master list of artists in the database
    '''
    pass
    
class Genera(tuple):
    '''
    this is the master list of generas (pre-loaded and immutable)
    '''
    def __new__(cls):
        return tuple.__new__(cls, ('Pop','Soul','Dance','Rap','Dancehall','Lovers Rock','Christian','Soca','Rock','Country') )
        
    

class Accessor(object):
    '''
    gets the database off disk and later re-saves it (if that is required)
    did some testing by hand
    todo: implement unit tests for this
    '''
    def __init__(self):
        try:
            with open(dblocation,'rb') as f:
                unpickler = pickle.Unpickler(f)
                self._app = unpickler.load()
        except IOError: #    if it had trouble opening the file we need to create one
            #   create the database
            with open(dblocation,'w') as f:
                #   create the database proper, it's underlying dictionary
                app = {}
                app['canon'] = []   # the canonical list of videos
                app['generas'] = Genera()    # the generas  
                app['artists'] = Artist()   #  the artists
                app['indexes'] = {}         #   the dictionary of indexes
                app['indexes']['alpha'] = []#   the alphabetical index is a list
                app['indexes']['genera'] = {}#   this dictionary entry contains a dictionary of genera lists
                #   now pickle this dictionary and dump it to our database file
                pickler = pickle.Pickler(f)    #   first create the pickler
                pickler.dump(app)   #   now dump our dictionary into the database file
                self._app = app
            
    
    def get_database(self):
        '''
        returns our dictionary
        '''
        return self._app
    
    def set_database(self,app):
        '''
        replaces the dictionary on disk, use with extreme care!
        todo: one day come back and add versioned backups
        '''
        with open(dblocation,'w') as f:
            #   pickle the dictionary and dump it to our database file
            pickler = pickle.Pickler(f)    #   first create the pickler
            pickler.dump(app)   #   now dump our dictionary into the database file
        self._app = app
        
    