#!/usr/bin/env python

'''

a little module I originally wrote to experiment with unpickling a directory 
listing (a collection of song objects) from some text in an entry in a redis 
database. 

later modified it to use reference to said database to control player.py via 
the philbus module's Transmitter() object

and now it controls vlc (run by player.py), playing and stopping videos 
controlling player.py

works w/ player.py


working on the event processor loop for this process cmdloop

was working on the code in the eventloop that changes mode managers 0 and 1
was going to put those in in the beginning of the suite that runs if
the mode is WUSS
'''

from Tkinter import *
from time import sleep
import threading
import subprocess
import os.path
import pickle
import redis
import philbus
import vjbmm
import Queue
import pprint
import dbengine
import logging
from logging.handlers import SysLogHandler



class MSList(Listbox):
    '''
    implements the master song list
    my listbox object
    '''
    def __init__(self,*arg,**args ):
        args['background']='black'
        args['foreground']='green'
        #args['font']=("Times",25)
        Listbox.__init__(self,*arg,**args)
    
    def init(self):
        '''
        my initialization routine, gets the list of songs 
        out the database and installs them
        '''
        loA = dbengine.Accessor()
        db = loA.get_database()

        for song in db['indexes']['alpha']:
            self.insert(END,song.name)
        #   now highlight the first song 
        self.selection_set(0)
        self._sel = 0
        self._songs = db

            
    def down(self):
        '''
        move the selection down
        '''
#        sel = self.getSelIdx()
        if self._sel < self.size() - 1:
            self.selection_clear(self._sel)
            self._sel = self._sel + 1
            self.see(self._sel)
            self.selection_set(self._sel)

    def up(self):
        '''
        move the selection up
        '''
        #sel = self.getSelIdx()
        if self._sel > 0:
            self.selection_clear(self._sel)
            self._sel = self._sel - 1
            self.see(self._sel)
            self.selection_set(self._sel)
            
    
    def getSelSong(self):
        '''
        get the selected song
        '''
        return self._songs['indexes']['alpha'][self._sel]

class CreditIndicator(LabelFrame):
    '''
    todo: test all this
    '''
    def __init__(self,*arg,**args ):
        #args['width']=500
        args['background']='black'
        args['foreground']='green'
        LabelFrame.__init__(self,*arg,**args)
        #   the actual variable that holds the credit amount
        self._txt = StringVar()
        self.label =  Label(self,textvariable = self._txt,justify = LEFT,font=("Times",90),background='black',fg='green')
        self.label.grid()
        self._cp = philbus.CreditPersister()
        self._credit = self._cp.account()
        self._txt.set(str(self._credit))
        
    def inc(self):
        self._credit = self._credit + 1
        self._txt.set(str(self._credit))    #   now display the increase in credit
        self._cp.inc()  #   persist the increment
        
    def dec(self):
        if self._credit > 0:
            self._credit = self._credit - 1
            self._txt.set(str(self._credit))    #   now display the decrease in credit
            self._cp.dec()  #   persist the decrement
            if self._credit == 0:
                self._cp.zero() #   persist the fact that the indicator is at zero
        
    def _getCredit(self):
        '''
        returns the amount of credit on the system
        '''
        return self._credit
    credit = property(_getCredit)
    
    

class Messages(LabelFrame):
    '''
    implements the box where the messages are displayed
    todo: test all this
    '''
    def __init__(self,*arg,**args ):
        args['background']='black'
        args['foreground']='green'
        LabelFrame.__init__(self,*arg,**args)
        #   the actual variable that holds the credit amount
        self._txt = StringVar()
        self.label =  Label(self,textvariable = self._txt,justify = LEFT,font=("Times",25),bg='black',fg='green')
        self._txt.set('welcome!')
        self.label.grid()
        
    def _setMessage(self,message):
        '''
        sets the message
        '''
        self._txt.set(message)    #   now display the new message
        
    def _getMessage(self):
        '''
        returns the message
        '''
        return self._txt.get()
    message = property(_getMessage,_setMessage)
    
    def setMessage(self,message):
        '''
        put this here because the property wasn't working and I'm not
        going to spare the time to diagnose it right now
        '''
        self._txt.set(str(message))    #   now display the new message



class vjbPlaylist(Listbox):
    '''
    implements the playlist
    a modification of the master song list object
    
    externally, has only two operations:
    
    queueSong
        adds a song to the playlist (the bottom)
        also adds it to the bottom of the  list of songs this widget displays
    getCurSong
        return the song at the top of the playlist, the 
        next in line for deletion
        
    and one operation that just returns data:
    
    isEmpty()
        returns true if the playlist is empty
    delCurSong()
        deletes the current song (if any), a.k.a the currently 
        playing song, a.k.a the next in line for deletion
    '''
    def __init__(self,*arg,**args ):
        args['background']='black'
        args['foreground']='green'
        Listbox.__init__(self,*arg,**args)
        self._songlist = []
        self._pp = philbus.PlaylistPersistor()
        self._initialized = False
        self._pp.account(self)
        self._initialized = True
        
    
    def queueSong(self,song):
        '''
        adds a song to the playlist (the bottom)
        '''
        self._songlist.append(song)
        self.insert(END,song.name)
        if self._initialized:
            #   if the module is initialized
            self._pp.queue(song)#   persist this event
        #   now highlight the first song 
        #self.selection_set(0)

            
        
    def isEmpty(self):
        '''
        returns true if the playlist is empty
        '''
        if self._songlist:
            return False
        else:
            self._pp.empty()
            return True
        
    
    def getCurSong(self):
        '''
        return the song at the top of the playlist, the 
        next in line for deletion
        '''
        if self._songlist:
            return self._songlist[0]
        
    def delCurSong(self):
        '''
        deletes the current song (if any), a.k.a the currently 
        playing song, a.k.a the next in line for deletion
        '''
        if self._songlist:
            self._songlist.pop(0)   #   delete it from the list of songs
            self.delete(0)  #   delete it from the list on screen
            self._pp.delete()
            
        


class Application(Frame):
    def __init__(self, master=None):
        # before we do anything else, create logger
        self.logger = logging.getLogger('vjb-controller')
        self.logger.setLevel(logging.DEBUG)
        # create handler and set level to debug
        ch = logging.handlers.SysLogHandler()
        ch.setLevel(logging.DEBUG)
        # create formatter
        formatter = logging.Formatter('%(pathname)s [%(process)d] : %(message)s')
        # add formatter to ch
        ch.setFormatter(formatter)
        # add ch to logger
        self.logger.addHandler(ch)

        self.logger.debug('controller started')# log first message
        Frame.__init__(self, master,background='black')
        self.grid()

        #   initiate the philbus communication objects
        #self._ot = philbus.ObjectTransceiver()
        self._xmitter = philbus.Transmitter()
        self.bus = philbus.Receiver()

        #      create and start the command loop thread
        self.cmdThrd = threading.Thread(target = self.cmdloop)
        self.cmdThrd.setDaemon(True)
        self.cmdThrd.start()

        
        #       the minimum amount of setup has been achieved, we can now send
        #       a command and have the event loop perform the rest of 
        #       initialization
        nextcmd = philbus.Command('init')#   make the command
        self._xmitter.publish(nextcmd)#  put it on the wire


    def createWidgets(self):
        #   create the message box
        self.msgs = Messages(self, height= 750, width=8, labelanchor='nw', text='messages',padx=80)
        #   create the cedit indicator
        self.credi = CreditIndicator(self,height= 190, width=8,labelanchor='nw',text='credit',padx=10)
        #   create the master song list
        self.mList = MSList(self, height=11, width=35, selectmode=SINGLE, font=("Times",30),selectforeground='#fc0',selectbackground='#333')
        #   create the playlist
        self.playList = vjbPlaylist(self,height=7, width=69,selectmode=SINGLE,font=("Times",15))
        #self.playList.configure(padx=5)
        #   grid everything
        self.msgs.grid(column = 0,row = 0,sticky=N+S+W+E)
        self.msgs.grid_propagate(0)
        self.credi.grid(column = 0,row = 1,sticky=N+S+W+E)
        self.credi.grid_propagate(0)
        self.mList.grid(column=1,row=0)
        self.playList.grid(column = 1,row=1)



    def cmdloop(self):
        '''
        this is started and run in an independent thread
        '''
        mode = vjbmm.MMM()
        mode0 = vjbmm.SMM()
        for cmd in self.bus.listen():
            pp = pprint.PrettyPrinter(depth = 6,indent=4)
            #pp.pprint(cmd)
            try:
                cmdObj = pickle.loads(cmd['data'])
            except (pickle.PickleError,TypeError):
                #   if we've reached here, unpickling was unsuccessful so the data from the bus is not a pickle
                print 'pickle error! \n'
                if isinstance(cmd['data'],long):
                    print 'not Song, long\n'
                elif cmd['data'] == 'hi':
                    print 'hi!\n'
                else :
                    print 'default\n'
                continue   #   this line marks the end of this 'except' suite
            #    if we've reached here there were no errors unpickling, so..
            #   okay, now we get into the mode suites, 
            if mode.currModeShortName() == 'INIT':
                print 'INIT'
                #   all code in this suite executes if this process is in 'Initialization' mode
                if cmdObj.command == 'init':
                    print '.init'
                    #   initialization code goes in this suite
                    #   first create the widgets
                    self.createWidgets()    
                    #   bind the events to the widgets now
                    self.bind_all("i",self._coinin)
                    self.bind_all("<Down>",self._down)
                    self.bind_all("n",self._up)
                    self.bind_all("<Right>",self._right)
                    self.bind_all("<Left>",self._left)
                    #   get and unpickle the collection from the redis database
                    #self.songs = self._ot.get()
                    #   initialize the listbox
                    self.mList.init()
                    
                    if self.credi.credit > 0:
                        #   the credit persistence system at work!
                        #   if there is credit on the indicator
                        #   issue a command to change mode to WUSS
                        nextcmd = philbus.Command('chmm','WUSS')#   make the command
                        nextcmd.source = 'cnt init .init'
                        self._xmitter.publish(nextcmd)#  put it on the wire
                    else:
                        #   else there is no credit on the indicator
                        #   proceed as before
                        #   issue a command to change mode to wait for credit mode
                        nextcmd = philbus.Command('chmm','WFCR')#   make the command
                        nextcmd.source = 'cnt init .init'
                        self._xmitter.publish(nextcmd)#  put it on the wire
                    
                        
                    
                if cmdObj.command == 'chmm':
                    print '.chmm'
                    #   if a command to change mode
                    if cmdObj.mode == 'WFCR':
                        print '..WFCR'
                        mode.changeMode('WFCR')
                        #   issue a command to initialize when sysytem gets to wait for credit mode
                        nextcmd = philbus.Command('init')#   make the command
                        nextcmd.source = 'cnt init .chmm'
                        self._xmitter.publish(nextcmd)#  put it on the wire
                    elif cmdObj.mode == 'WUSS':
                        print '..WUSS'
                        mode.changeMode('WUSS')
                        #   issue a command to initialize when sysytem gets to wait for credit mode
                        nextcmd = philbus.Command('init')#   make the command
                        nextcmd.source = 'cnt init .chmm'
                        self._xmitter.publish(nextcmd)#  put it on the wire
            elif mode.currModeShortName() == 'WFCR':
                print 'WFCR'
                #   and all the code in this suite executes if mode is 'wait for credit mode'
                if cmdObj.command == 'init': #   if we got an 'init' command off the wire
                    print ' init'
                    #self.msgs.message = 'Please insert $20 coin(s), one coin = one credit'
                    self.msgs.setMessage('Please insert\n $20 coin(s)\n one coin =\n one credit')
                    if not self.playList.isEmpty():
                        #   if the playlist is not empty
                        if mode0.currModeShortName() == 'STPM':
                            #   and mode0 is in stop mode
                            print '..STPM'
                            song = self.playList.getCurSong()  #   get the uppermost song object
                            #   give the player the command to activate vlc
                            cmd = philbus.Command('play',song)
                            cmd.source = 'cnt wfcr init stpm'
                            self._xmitter.publish(cmd)   #   put the command on the wire
                    else:
                        cmd = philbus.Command('AIDLMD')
                        cmd.source = 'cnt wfcr init'
                        self._xmitter.publish(cmd)   #   put the command on the wire
                    #print ' init wfcr'
                elif cmdObj.command == 'cnin':#   if a coin was dropped in the slot
                    print '.cnin'
                    self.credi.inc()    #   increment the credit indicator
                    #print 'wfcr cnin'
                    #   issue a command to change mode to waiting for user to select a song mode
                    cmd = philbus.Command('DIDLMD')  #   
                    cmd.source = 'cnt wfcr .vlcs playList.is-not-Empty(:-))'
                    self._xmitter.publish(cmd)   #   put the command on the wire
                    nextcmd = philbus.Command('chmm','WUSS')#   make the command
                    nextcmd.source = 'cnt wfcr .cnin'
                    self._xmitter.publish(nextcmd)#  put it on the wire
                elif cmdObj.command == 'vlcs':#   if vlc has stopped
                    print '.vlcs'
                    self.playList.delCurSong()
                    if not self.playList.isEmpty():
                        #if there is a next Song
                        print '.self.playList.is-not-Empty(:-))'
                        song = self.playList.getCurSong()
                        print '.song:', song.name
                        cmd = philbus.Command('play',song)  #   make up a play command with it as the argument
                        cmd.source = 'cnt wfcr .vlcs playList.is-not-Empty(:-))'
                        self._xmitter.publish(cmd)   #   put the command on the wire
                    else:
                        #else the playlist is empty, so just change mode0 to STPM and print the debugging
                        mode0.changeMode('STPM')
                        print '.self.playList.is-completely-Empty(:-))'
                        cmd = philbus.Command('AIDLMD')  #   make up a play command with it as the argument
                        cmd.source = 'cnt wfcr .vlcs playList.is-not-Empty(:-))'
                        self._xmitter.publish(cmd)   #   put the command on the wire
                if cmdObj.command == 'chmm':
                    print '.chmm'
                    #   if a command to change mode
                    if cmdObj.mode == 'WUSS':
                        print '..WUSS'
                        mode.changeMode('WUSS')
                        #   issue a command to initialize when sysytem gets to waiting for user to select a song mode
                        nextcmd = philbus.Command('init')#   make the command
                        nextcmd.source = 'cnt wfcr chmm'
                        self._xmitter.publish(nextcmd)#  put it on the wire
            elif mode.currModeShortName() == 'WUSS':
                print 'WUSS'
                #   and all the code in this suite executes if mode is 'waiting for user to select a song mode'
                
                #   mode 0 and 1 manager update code
                if cmdObj.command == 'chm0':
                    print '.chm0'
                    #   if a command to change mode
                    if cmdObj.mode == 'STPM':
                        print '..STPM'
                        mode0.changeMode('STPM')
                    #   if a command to change mode
                    elif cmdObj.mode == 'PLYM':
                        print '..PLYM'
                        mode0.changeMode('PLYM')
                    continue
                #   the regular command processing code goes from here on
                if cmdObj.command == 'chmm':
                    print '.chmm'
                    #   if a command to change mode
                    if cmdObj.mode == 'WFCR':
                        #   if a command to change main mode back to WFCR
                        print '..WFCR'
                        mode.changeMode('WFCR')
                        #   issue a command to initialize when sysytem gets to WFCR mode
                        nextcmd = philbus.Command('init')#   make the command
                        nextcmd.source = 'cnt wuss chmm'
                        self._xmitter.publish(nextcmd)#  put it on the wire
                elif cmdObj.command == 'init': #   if we got an 'init' command off the wire
                    print '.init'
                    #self.msgs.message = 'Please insert $20 coin(s), one coin = one credit'
                    self.msgs.setMessage('use the up and\ndown buttons\nto move the\nselection to\nthe video you\nwant to see\nthen press the\nselect button\nto see it\n\nto request\nmore videos\ncall 567-4142')
                    if not self.playList.isEmpty():
                        #   if the playlist is not empty
                        print '..playlist not empty'
                        if mode0.currModeShortName() == 'STPM':
                            #   and mode0 is in stop mode
                            print '..STPM'
                            song = self.playList.getCurSong()  #   get the uppermost song object
                            #   give the player the command to activate vlc
                            cmd = philbus.Command('play',song)
                            cmd.source = 'cnt wfcr init stpm'
                            self._xmitter.publish(cmd)   #   put the command on the wire
                    #print ' init wuss'
                elif cmdObj.command == 'cnin':#   if a coin was dropped in the slot
                    print '.cnin'
                    self.credi.inc()    #   increment the credit indicator
                    #print 'wuss cnin'
                elif cmdObj.command == 'dbtn':#   if the down button was pressed
                    print '.dbtn'
                    self.mList.down()
                    #print 'wuss dbtn'
                elif cmdObj.command == 'ubtn':#   if the up button was pressed
                    print '.ubtn'
                    self.mList.up()
                    #print 'wuss ubtn'
                elif cmdObj.command == 'rbtn':#   if the right button was pressed
                    print '.rbtn'
                    if mode0.currModeShortName() == 'STPM':
                        #   if mode0 is in stop mode
                        print '..STPM'
                        song = self.mList.getSelSong()  #   get the selected song object
                        #   add the song to the playlist
                        self.playList.queueSong(song)
                        #   give the player the command to activate vlc
                        cmd = philbus.Command('play',song)
                        cmd.source = 'cnt wuss rbtn stpm'
                        self._xmitter.publish(cmd)   #   put the command on the wire
                    else:
                        #   if mode0 is in play mode
                        print '..PLYM'
                        song = self.mList.getSelSong()  #   get the selected song object
                        #   add the song to the playlist
                        self.playList.queueSong(song)#  just queue up the song, don't issue 
                        #         a command to play it since there's already a song playing
                    self.credi.dec()
                    if self.credi.credit == 0:
                        #   issue a command to change mode to wait for credit mode
                        nextcmd = philbus.Command('chmm','WFCR')#   make the command
                        nextcmd.source = 'cnt wuss rbtn (self.credi.credit == 0)'
                        self._xmitter.publish(nextcmd)#  put it on the wire
                elif cmdObj.command == 'stop':#   if the left button was pressed
                    print '.stop (no actual direct action on this side)'
                    #   the action for this event, though sent from a button here, happens entirely on the 
                    #   player, after the player stops and this module recieves notification of that event,
                    #   then this module will respond to that.
                    #print 'wuss stop'
                elif cmdObj.command == 'vlcs':#   if vlc has stopped
                    print '.vlcs'
                    self.playList.delCurSong()
                    if not self.playList.isEmpty():
                        #if there is a next Song
                        print '.self.playList.is-not-Empty(:-))'
                        song = self.playList.getCurSong()
                        print '.song:', song.name
                        cmd = philbus.Command('play',song)  #   make up a play command with it as the argument
                        cmd.source = 'cnt wuss .vlcs playList.is-not-Empty(:-))'
                        self._xmitter.publish(cmd)   #   put the command on the wire
                    else:
                        #else the playlist is empty, so just change mode0 to STPM and print the debugging
                        mode0.changeMode('STPM')
                        print '.self.playList.is-completely-Empty(:-))'
                        
                else:
                    print '.default: cmd.command = ',cmdObj.command,'source:',cmdObj.source,
                    if cmdObj.command == 'chm0':
                        print cmdObj.mode
                    else:
                        print ''
                    #print 'wuss .vlcs'
            #           end of event processor

    def _coinin(self,evnt):
        '''
        this is fired when the user presses the 'w' button, the coinin
        button!
        '''
        cmd = philbus.Command('cnin')
        self._xmitter.publish(cmd)   #   put the command on the wire
        
    def _down(self,evnt):
        '''
        this is fired when the user presses the down arrow, the down
        button!
        '''
        cmd = philbus.Command('dbtn')
        self._xmitter.publish(cmd)   #   put the command on the wire

    def _up(self,evnt):
        '''
        this is fired when the user presses the up arrow, the up
        button!
        '''
        cmd = philbus.Command('ubtn')
        self._xmitter.publish(cmd)   #   put the command on the wire

    def _right(self,evnt):
        '''
        this is fired when the user presses the right arrow, the select
        button!
        '''
        cmd = philbus.Command('rbtn')
        self._xmitter.publish(cmd)   #   put the command on the wire

    def _left(self,evnt):
        '''
        this is fired when the user presses the left arrow, the reject
        button.
        '''
        cmd = philbus.Command('stop')
        self._xmitter.publish(cmd)   #   put the command on the wire

        
    def _stopPlayback(self):
        '''
        handler for the stop playback button
        '''
        cmd = philbus.Command('stop')
        self._xmitter.publish(cmd)   #   put the command on the wire



sleep(1)
app = Application()
app.master.title("controller")
w, h = 1280, 1024 # app.winfo_screenwidth(), app.winfo_screenheight()
t = app.winfo_toplevel()
t.configure(takefocus=True,background='black')
t.geometry("%dx%d+0+0" % (w, h))
#t.overrideredirect(1)

app.mainloop()
