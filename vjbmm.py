'''
the Video Juke Box Mode Managers 

'''

class MM1(object):
    '''
    the video juke box main mode manager for the new mode1
    What I did was having developed this for the player, and the main mode,
    cut and pasted all the code from MMM into this. 
    then just changed the modelist to suit
    '''
    def __init__(self):
        self._modelist = (('EMTY','Empty'),('NMTY','Not Empty'))#    the names of the modes go here
        self._mode = 0  #   the numeric code of the default mode, this one is the default mode
        
    def currModeLongName(self):
        '''
        returns the long name of the current mode
        '''
        return self._modelist[self._mode][1]
    
    
    def currModeShortName(self):
        '''
        returns the short name of the current mode
        '''
        return self._modelist[self._mode][0]
        
    def currModeNumber(self):
        '''
        returns the numeric code of the current mode
        '''
        return self._mode
    
    def changeMode(self,newmode):
        '''
        changes the current mode to the mode passed in as an argument
        '''
        #   validate the input
        found = False
        if isinstance (newmode,str):
            #   if a string was passed in
            for idx,mode in enumerate(self._modelist):
                #print mode
                if newmode in mode:#    this will validate if either the long name or short name is found
                    #   we've found the mode
                    self._mode = idx
                    found = True
            if not found:
                raise ValueError('not a valid mode')
        else:
            #   else a number was passed (hopefully! come back and make this more robust)
            if newmode in range(len(self._modelist)):
                self._mode = newmode
            else:
                raise ValueError('not a valid mode')


class MMM(object):
    '''
    the video juke box main mode manager proper
    What I did was having developed this for the player, cut and pasted
    all the code from SMM into this. then just changed the modelist to suit
    '''
    def __init__(self):
        self._modelist = (('INIT','Initialization'),('WFCR','Waiting For Credit'),
                        ('WUSS','Waiting For User To Select A Song'))#    the names of the modes go here
        self._mode = 0  #   the numeric code of the default mode, this one is the default mode
        
    def currModeLongName(self):
        '''
        returns the long name of the current mode
        '''
        return self._modelist[self._mode][1]
    
    
    def currModeShortName(self):
        '''
        returns the short name of the current mode
        '''
        return self._modelist[self._mode][0]
        
    def currModeNumber(self):
        '''
        returns the numeric code of the current mode
        '''
        return self._mode
    
    def changeMode(self,newmode):
        '''
        changes the current mode to the mode passed in as an argument
        '''
        #   validate the input
        found = False
        if isinstance (newmode,str):
            #   if a string was passed in
            for idx,mode in enumerate(self._modelist):
                #print mode
                if newmode in mode:#    this will validate if either the long name or short name is found
                    #   we've found the mode
                    self._mode = idx
                    found = True
            if not found:
                raise ValueError('not a valid mode')
        else:
            #   else a number was passed (hopefully! come back and make this more robust)
            if newmode in range(len(self._modelist)):
                self._mode = newmode
            else:
                raise ValueError('not a valid mode')

class SMM(object):
    '''
    the video juke box sub mode manager 
    '''
    def __init__(self):
        self._modelist = (('STPM','Stopped Mode'),('PLYM','Play Mode'))#    the names of the modes go here
        self._mode = 0  #   the numeric code of the current mode, this one is the default mode
        
    def currModeLongName(self):
        '''
        returns the long name of the current mode
        '''
        return self._modelist[self._mode][1]
    
    
    def currModeShortName(self):
        '''
        returns the short name of the current mode
        '''
        return self._modelist[self._mode][0]
        
    def currModeNumber(self):
        '''
        returns the numeric code of the current mode
        '''
        return self._mode
    
    def changeMode(self,newmode):
        '''
        changes the current mode to the mode passed in as an argument
        '''
        #   validate the input
        found = False
        if isinstance (newmode,str):
            #   if a string was passed in
            for idx,mode in enumerate(self._modelist):
                #print mode
                if newmode in mode:#    this will validate if either the long name or short name is found
                    #   we've found the mode
                    self._mode = idx
                    found = True
            if not found:
                raise ValueError('not a valid mode')
        else:
            #   else a number was passed (hopefully! come back and make this more robust)
            if newmode in range(len(self._modelist)):
                self._mode = newmode
            else:
                raise ValueError('not a valid mode')
