'''
this is my message bus transmitter, receiver and command object

archived after my second failed attempt at modifying the Receiver() object.. giving up!

was working on the Command class

todo: write tests for this module!
'''
import pickle
import redis
import os
import types
import datetime
import dbengine
import logging
from logging.handlers import SysLogHandler


class Song(object):
    '''
    song object
    
    this is the datum that's at the heart of the database which is itself at 
    the heart of the system. takes the full pathmname in the constuctor
    ansd automatically genertates it's remaining atributes from that. 
    this object has served well, not it's time for a new song object!
    enter the video object!
    
    attributes:
        version
            the version of this object
        screen name
            name as it appears to the user
        filename
            
        full pathname
            
            
    constructor
        takes the full pathname and parses it for the abovementioned attributes
    '''
    def __init__(self,fullpath):
        self.version = 1
        self._fpath = fullpath
        self._filename = os.path.basename(fullpath)
        self._screenname = os.path.splitext(self._filename)[0]
        self._screenname = self._screenname.replace("_", " ")
        
    def _retfpath(self):
        return self._fpath
    fullpath = property(_retfpath)
    
    def _retfname(self):
        return self._filename
    filename = property(_retfname)

    def _retscreenname(self):
        return self._screenname
    screenname = property(_retscreenname)

class Command(object):
    '''
    instances of this class are pickled and passed on the message bus 
    as commands to the system
    
    has a fixed set of commands, one of which is passed in on instantiation
    
    commands: 
        play
            the video to play, if this is the command you're passing into the 
            constructor, also pass a second argument which is the song object
            relating to the video to  be played will be exposed in the instance
            as command.song
        stop
            stop the currently playing video
        chm0
            change mode0, pass a second textual argument, the mode to change to
            initializes the mode attribute to the value of this argument
        vlcs
            not really a command, actually an event notification, notify the 
            rest of the system that the process in which vlc was running has 
            just now ceased to exist.
        init
            command to initialize the system, sent from conmtroller to make 
            sure it has a command in the quueue so when it starts it's 
            event processing loop, it will perform the first set of 
            actions
        chmm
            change main mode, pass a second textual argument, the mode to change to
            initializes the instance's mode attribute to the value of this argument
        cnin
            a coin was dropped in the slot
        dbtn
            the down button was pressed
        ubtn
            the up button was pressed
        rbtn    
            the right (select) button was pressed
        chm1
            change mode1, pass a second textual argument, the mode to change to
            initializes the mode attribute to the value of this argument
         AIDLMD
            activate idle mode
         DIDLMD
            deactivate idle mode
    
    todo:
    put in validation code for the values passed to the constructor
    '''
    def __init__(self,*args):
        self.cmd = ('play','stop','chm0','vlcs','init','chmm','cnin','dbtn','ubtn','rbtn','AIDLMD','DIDLMD')   #   these are all the commands and events the system generates
        #cmd = cmd.tolower()
        cmd = args[0]
        if cmd in self.cmd: #    make sure the first argument is a valid command
            self.command = cmd
            if cmd == 'play': #    if the command is to play a video
                self.song = args[1]    #   bring in the second argument and 
                #   put it in this object as the 'song' attribute 
            elif cmd == 'chm0':
                self.mode = args[1]
            elif cmd == 'chmm':
                self.mode = args[1]
            elif cmd == 'chm1':
                self.mode = args[1]
            
        else:
            #   else the command is unfamiliar
            raise ValueError('unfamiliar command')
            
    
class Transmitter(object):
    '''
    my message bus object that puts messages on the bus
    '''
    
    def __init__(self):
        self._r = redis.StrictRedis(host='localhost', port=6379, db=0)    #   initialize redis client
        
    def publish(self,ob):
        '''
        this publishes our data on the bus
        '''
        self._r.publish('cchan',pickle.dumps(ob))
        
class Receiver(object):
    '''
    this is my class that receives messages off my message bus
    
    initializes itself, all you have to do is call the listen() method from
    your for loop to get the generator and that's it!
    '''
    def __init__(self):
        self._r = redis.StrictRedis(host='localhost', port=6379, db=0)    #   initialize redis client
        self._p = self._r.pubsub()
        self._p.subscribe('cchan')
        
    
    def listen(self):
        import types
        a = self._p.listen()    #    first put the generator in a variable so we can put it into our custom object
        return a
#        b = a.next()    #   call the original next() method and store it's return in 'b'
#        if isinstance(b['data'],str):
            #   
#            yield loads(b['data']) #   unpickle the string and return it
#        else:
#            yield b['data']    #    else just return what we got off the wire


class ObjectTransceiver(object):
    '''
    my message bus object that puts and gets objects off the bus
    '''
    
    def __init__(self):
        self._r = redis.StrictRedis(host='localhost', port=6379, db=0)    #   initialize redis client
        
    def put(self,ob):
        '''
        this puts our object on the bus
        '''
        self._r.set('vjb:video-dir-pickle',pickle.dumps(ob,0))

    def get(self):
        '''
        this gets our object off the bus and returns it
        '''
        return pickle.loads(self._r.get('vjb:video-dir-pickle'))

class CreditPersister(object):
    '''
    my class that handles the persisting of credit inc and dec events
    and the accounting of this persisted record of events
    
    in the 
    
    Properties
    
    Methods
    inc
        persist an inc
    dec
        persist a dec
    zero
        persist the fact that the indicator is at zero
    account
        go back through the list and see how much credit should be
        on the system now
    '''
    def __init__(self):
        '''
        '''
        self._r = redis.Redis(host='localhost')

        # early in the process, create logger
        self.logger = logging.getLogger('vjb:philbus:CreditPersister')
        self.logger.setLevel(logging.DEBUG)
        # create handler and set level to debug
        ch = logging.handlers.SysLogHandler()
        ch.setLevel(logging.DEBUG)
        # create formatter
        formatter = logging.Formatter('[%(process)d]: %(name)s : %(message)s')
        # add formatter to ch
        ch.setFormatter(formatter)
        # add ch to logger
        self.logger.addHandler(ch)

        
    def inc(self):
        '''
        called when you want to register an increment
        '''
        self._r.lpush('vjb:creditindicator',
            datetime.datetime.now().isoformat() + ';+1')

    def dec(self):
        '''
        called when you want to register a decrement
        '''
        self._r.lpush('vjb:creditindicator',
            datetime.datetime.now().isoformat() + ';-1')
            
    def zero(self):
        '''
        called when you want to register the fact that the indicator is 
        at zero
        '''
        self._r.lpush('vjb:creditindicator',
            datetime.datetime.now().isoformat() + ';0')
            
    def account(self):
        '''
        returns the amount of credit that was on the system before the plug
        was pulled. 
        it does that by going back through the list (focusing on the credit
        field). if the first entry is a zero, then that means the system was 
        properly shut down. credit on the system is zero. but if it was either
        -1 or +1 then create a python list, go back through the redis list
        and add each entry you find to the python list until you reach the 
        next zero. 
        
        '''
        llList = [] #   create the list object we're going to be using 
        liIndex = 0
        while 1:
            #   first get the database item,
            #   partition the string we get out our database into the date part 
            #   and the amount-of-credit part, returning it into a packed tuple
            if self._r.lindex('vjb:creditindicator',liIndex):
                #   protect against a mal-formed redis list
                ltDatum = self._r.lindex('vjb:creditindicator',liIndex).partition(';')
                if ltDatum[2] != '':
                    # if we got back something in the amount-of-credit part
                    if ltDatum[2] != '0':#    if the last recorded event was not a zero event
                        if ltDatum[2] == '-1':
                            llList.append(-1)
                        elif ltDatum[2] == '+1':
                            llList.append(1)
                    else:
                        #   if the recorded event was a zero
                        #   todo come back and fix this- just in case we come upon
                        #   something aside from a -1, a +1 or a zero here
                        if liIndex == 0:
                            # if we're at the first item on the stack
                            return 0    #   return zero
                        else:
                            #   else we're at least one item into the stack
                            #   this means we have a list here of +1's and -1's
                            #   so all we need to do now is go through them from
                            #   first to last, perform a running tally and the
                            #   result will be the amount of credit that was on 
                            #   the system before it was shut down
                            liTally = 0
                            for a in llList:
                                liTally += a
                            #   by the time we reach here liTally contains a running
                            #   total of the +1 and -1's in the stack
                            return liTally
                            #   here
                        return 0    #   return zero
                else:
                    #   if we got nothing in the amount-of-credit part
                    #   put a logging facxility here to log this error
                    return 0
                liIndex += 1#   
            else:
                return 0
        #    if we've reached here we have a list of +1's and -1's
        #    go through the python list 
        #   from the start tallying up the +1's and -1's. return the total
        
class PlaylistPersistor(object):
    '''
    my class that handles the persisting of playlist queue and delete events
    and the accounting of this persisted record of events
    
    
    Properties
    
    Methods
    queue
        persist a song
    delete
        persist a delete of a song from the playlist
    empty
        persist the fact that the playlist is empty
    account
        go back through the list and see which songs should be
        in the playlist now
    '''
    def __init__(self):
        '''
        '''
        self._r = redis.Redis(host='localhost')
        self._db = dbengine.Accessor()
        # early in the process, create logger
        self.logger = logging.getLogger('vjb:philbus:PlaylistPersistor')
        self.logger.setLevel(logging.DEBUG)
        # create handler and set level to debug
        ch = logging.handlers.SysLogHandler()
        ch.setLevel(logging.DEBUG)
        # create formatter
        formatter = logging.Formatter('[%(process)d]: %(name)s : %(message)s')
        # add formatter to ch
        ch.setFormatter(formatter)
        # add ch to logger
        self.logger.addHandler(ch)
        
    def queue(self,video):
        '''
        called when you want to register an enqueing 
        '''
        self._r.lpush('vjb:playlist',
            datetime.datetime.now().isoformat() + ';' + str(video.index))

    def delete(self):
        '''
        called when you want to register a deletion from the playlist
        '''
        self._r.lpush('vjb:playlist',
            datetime.datetime.now().isoformat() + ';D')
            
    def empty(self):
        '''
        called when you want to register the fact that the playlist is 
        empty
        '''
        self._r.lpush('vjb:playlist',
            datetime.datetime.now().isoformat() + ';E')
            
    def account(self,playlist = None):
        '''
        takes the already initialized vjbPlaylist() from the running system
        and if there were songs in the playlist before the plug was pulled
        this method uses the playlist's queueSong() method to put those songs in there
        but if there were no songs in the playlist before then it 
        returns a zero
        
        it works by going back through the redis list (focusing on the video
        field). if the first entry is an E, then that means the system was 
        properly shut down. the playlist is empty. but if it was a number
        then create a python list, 
        go back through the redis list
        and add each entry you find to the python list until you reach the 
        next E. 
        
        if it returns as zero then there was an error 
        if it returns a 1 then we were successful, we either poulated the 
        playlist or there were no songs to populate the playlist with
        '''
        llFirstList = [] #   create the list object we're going to be using 
        llFinalList = []
        liIndex = 0
        while 1:
            #   first get the item from the redis list,
            #   partition the string we get into the date part 
            #   and the action part, returning it into a packed tuple
            if self._r.lindex('vjb:playlist',liIndex):#   protection against a mal-formed redis list
                ltDatum = self._r.lindex('vjb:playlist',liIndex).partition(';')#    split the current line in the redis list into the date part and the command part
                loCommand = ltDatum[2]# yes, take off the command part 
                if loCommand != '':# protection against an empty command (just in case it does happen..)
                    if str(loCommand).isdigit():#    if the command
                        #   was a number then it was an enqueued video
                        llFirstList.append(loCommand)    #   append it to our first python list
                    else:
                        #   if the command was not numeric
                        if loCommand == 'D':
                            #   if the command was to delete a song from the playlist
                            llFirstList.append(loCommand)    #   append it to our first python list 
                        elif loCommand == 'E':
                            #   else if the command was a signal that the 
                            #   playlist is now empty
                            if liIndex == 0:
                                # if we're at the first item on the redis list
                                return 1    #   return 1 and do nothing with 
                            #           the passed-in playlist, there were no 
                            #           entries in the playlist at plug-out time
                            else:
                                #   else we're at least one item into the stack
                                #   this means we have been bulilding 
                                #   our first python list and it
                                #   now contains an audit trail inserts and deletes
                                #   of the playlist between the last time it was empty 
                                #   up until the plug was pulled.
                                #   this first list is a series of numbers and 'D's
                                #   ordered from latest to earliest. 
                                #   
                                #   all we need to do now is reverse this list and 
                                #   loop through it using each command to create
                                #   the final list of videos
                                #   
                                #   each number in this final list
                                #   represents the index number of the video in the 
                                #   canonical
                                #   list of videos in the master database
                                #
                                #   so all we need to do now is go through them 
                                #   from
                                #   first to last, look up each one in the canon
                                #   (in the master database) and queue it up in
                                #   the passed-in playlist.
                                
                                #   use the instructions in the first list to 
                                #   create the list of numbers that index the videos
                                #   that were in the playlist
                                llFirstList.reverse()
                                for a in llFirstList:
                                    if a.isdigit():
                                        #   if this is a number append it to the final list
                                        llFinalList.append(a)
                                    else:
                                        #   else this is a delete command
                                        #   so pop the first video off the final list
                                        llFinalList.pop(0)
                                #   here we have afinal representastion of the
                                #   playlist as it was before the plug was pulled
                                
                                #   so now just use this list to er-populate the 
                                #   passed-in playlist object, and if there was none
                                #   then print out the list
                                    
                                    
                                if playlist:
                                    #   if ther was a passed-in playlist 
                                    #   object (I set it up this way so
                                    #   it would still work if we didn't
                                    #   pass in a playlist while debugging)
                                    for a in llFinalList:
                                        #   loop through the final list
                                        playlist.queueSong(self._db._app['canon'][int(a)])
                                else:
                                    for a in llFinalList:
                                        #   loop through the final list
                                        print self._db._app['canon'][int(a)].name
                                return 1
                else:
                    #   if we got nothing in the amount-of-credit part
                    #   put a logging facxility here to log this error
                    return 0
            else:
                #   we got back a None from the redist list
                return 0
            liIndex += 1#   last command to execute in the while loop
        #here

